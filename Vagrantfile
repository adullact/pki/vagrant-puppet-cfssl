$script_ubuntu = <<-SCRIPT
if [ ! -f /opt/puppetlabs/bin/puppet ]; then
  sudo wget --quiet https://apt.puppetlabs.com/puppet7-release-jammy.deb # Ubuntu 22.04
  sudo dpkg -i puppet7-release-jammy.deb                                 # Ubuntu 22.04
  sudo apt-get update
  sudo apt-get install puppet-agent
fi
SCRIPT

Vagrant.configure("2") do |config|

  # Default values
  default_host_postgresql_port = 5432
  default_host_cfssl_port      = 8888
  default_host_cfssl_ip        = "127.0.0.1"

  # Environment variable customizations
  host_postgresql_port = ENV['VAGRANT_HOST_POSTGRESQL_PORT'] ? ENV['VAGRANT_HOST_POSTGRESQL_PORT'] : default_host_postgresql_port
  host_cfssl_port      = ENV['VAGRANT_HOST_CFSSL_PORT']      ? ENV['VAGRANT_HOST_CFSSL_PORT']      : default_host_cfssl_port
  host_cfssl_ip        = ENV['VAGRANT_HOST_CFSSL_IP']        ? ENV['VAGRANT_HOST_CFSSL_IP']        : default_host_cfssl_ip

  config.vm.box = "ubuntu/jammy64" # Ubuntu 22.04
  config.vm.hostname = "pki.example.org"
  config.vm.network "forwarded_port", id: 'CfsslApi',   guest: 8888,  host: host_cfssl_port,      auto_correct: true, host_ip: host_cfssl_ip
  config.vm.network "forwarded_port", id: 'PostgreSQL', guest: 5432,  host: host_postgresql_port, auto_correct: true, host_ip: host_cfssl_ip
  config.vm.provider "virtualbox" do |vb|
    vb.name = "DEMO_CFSSL"
  # vb.memory = "4096"
  # vb.cpus = "4"
  end

  config.vm.synced_folder "puppet/hieradata/", "/tmp/vagrant-puppet/hieradata"
  config.vm.provision "shell", inline: $script_ubuntu
  config.vm.provision "puppet" do |puppet|
    # documentation
    # https://developer.hashicorp.com/vagrant/docs/provisioning/puppet_apply
    puppet.hiera_config_path = "puppet/hiera.yaml"
    puppet.working_directory = "/tmp/vagrant-puppet"
    puppet.module_path = "puppet/modules"
    puppet.manifest_file = "default.pp"
    puppet.manifests_path= "puppet/manifests"
    # puppet.options = "--debug"
  end

  # Message to show after vagrant up
  config.vm.post_up_message = <<-MESSAGE
  --------------------------------------------------------
  ⚠️   Created only for test or demonstration purposes. ⚠️

  ⚠️   No security has been made for production.        ⚠️
  --------------------------------------------------------


  CFSSL API       https://github.com/cloudflare/cfssl/blob/master/doc/
  --------------------------------------------------------------------
  /api/v1/cfssl/crl       Generates a CRL out of the certificate DB
  /api/v1/cfssl/info      Get information about certification authority (CA)
  /api/v1/cfssl/newcert   Generate a new private key and certificate
  /api/v1/cfssl/certinfo  Lookup a certificate's info
  /api/v1/cfssl/revoke    Revoke a certificate

  # Example: /api/v1/cfssl/info
  curl -s -d '{}' -H "Content-Type: application/json" -X POST #{host_cfssl_ip}:#{host_cfssl_port}/api/v1/cfssl/info

  --------------------------------------------------------------------
  Use 'vagrant port' command line to see port mapping.

  Default :  5432 (guest) --> #{host_postgresql_port} (host)  PostgreSQL   port
             8888 (guest) --> #{host_cfssl_port} (host)  CFSSL   API  port

  --------------------------------------------------------------------
  CFSSL API ..... http://#{host_cfssl_ip}:#{host_cfssl_port}/api/v1/cfssl/<endpoint>
  --------------------------------------------------------------------

  --------------------------------------------------------
  ⚠️   Created only for test or demonstration purposes. ⚠️
  --------------------------------------------------------
  MESSAGE
end
