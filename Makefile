##-------------------------------------------------------------------------------------------------------------------

up_VM: download_puppet_modules vagrant_up  ## Download required Puppet modules and start Vagrant VM
.PHONY: up_VM

stop_VM:  ## Stop Vagrant VM gracefully
	vagrant halt
.PHONY: stop_VM

remove_VM: vagrant_destroy clean_vagrant_directory clean_puppet_modules  ## Destroy Vagrant VM and remove VM directories
.PHONY: remove_VM


##
##-------------------------------------------------------------------------------------------------------------------

download_puppet_modules: clean_puppet_modules ## Download all required Puppet modules used to configure the virtual machine
	./BUILD.sh
.PHONY: download_puppet_modules

clean_puppet_modules: ## Remove Puppet modules directory
	rm -r puppet/modules  || exit 0
.PHONY: clean_puppet_modules

clean_vagrant_directory: ## Remove Vagrant directory
	rm -r .vagrant || exit 0
.PHONY: clean_vagrant_directory

##-------------------------------------------------------------------------------------------------------------------

vagrant_up: ## Start Vagrant VM, without download required Puppet modules
	vagrant up
	vagrant port
.PHONY: vagrant_up

vagrant_provision: ## Start Vagrant VM, without download required Puppet modules
	vagrant up --provision
	vagrant port
.PHONY: vagrant_provision

vagrant_destroy: ## Destroy Vagrant VM, without remove VM directories
	vagrant destroy -f || exit 0
.PHONY: vagrant_destroy

vagrant_port: ## Destroy Vagrant VM, without remove VM directories
	vagrant port
.PHONY: vagrant_port


##-------------------------------------------------------------------------------------------------------------------

# Default goal and help
.DEFAULT_GOAL := help
help: ## Show this help
	@echo "Makefile - Vagrant CFSSL (created only for test or demonstration purposes)"
	@echo ""
	@echo "Usage: make [target]"
	@echo "---------------------------"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
#	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | sed -e 's/^Makefile:\(.*\)/\1/' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
#---------------------------------------------#

