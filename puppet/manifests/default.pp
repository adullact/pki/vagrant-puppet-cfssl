
# Prerequired Puppet component modules
#########################################################################
include cfssl

# Timezone
##########################################################################

# Operating system use Europe/Paris timezone
exec { 'timedatectl set-timezone':
    command => "/usr/bin/timedatectl set-timezone Europe/Paris",
    unless  => "/usr/bin/timedatectl | grep Europe/Paris",
}
    ##########################################################################
    #     # Operating system use America/New_York timezone
    #     exec { 'timedatectl set-timezone':
    #         command => "/usr/bin/timedatectl set-timezone America/New_York",
    #         unless  => "/usr/bin/timedatectl | grep America/New_York",
    #     }
    ##########################################################################
