# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)


--------------------------------

## v1.6.0, 2024.07.11

### Added

- feat: add Makefile

### Changed

- chore: use puppet-cfssl `3.0.1` #1
- chore: update all other Puppet modules (concat, postgresql, stdlib, vcsrepo and archive) #2


--------------------------------

## v1.5.0, 2023.11.10

### Changed

- chore: use puppet-cfssl `2.1.1`


--------------------------------

## v1.4.0, 2023.05.31

### Added

- feat: allow access to CFSSL database from an IDE
- docs: warning ("Created **only** for **test** or **demonstration** purposes.")
- docs: add contributing file

### Changed

- fix(vagrant provision): use puppet7

### Fixed

- improve documentation


--------------------------------

## v1.3.0, 2023.04.03

### Added

- Feat: allow host to communicate with database port
- Docs: add "how to access CFSSL database from an IDE?"

### Changed

- Rename root and intermediate certificates
- Update puppet-concat (v7.3.1 instead of v7.1.1)
- Update puppet-stdlib (v8.5.0 instead of v8.1.0)

### Fixed

- add CRL configuration


--------------------------------

## v1.2.1, 2023.02.28

### Fixed

- use a puppet module of PostgreSQL compatible with Ubuntu 22.04


--------------------------------

## v1.2.0, 2023.02.27

### Added

- allow to configure CFSSL port and IP

### Changed

- use Ubuntu 22.04 instead of Ubuntu 20.04


--------------------------------

## v1.1.0, 2022.12.01

### Added

- Add Gitlab CI (linter, documentation)
- Add Gitlab templates (issue and MR)

### Changed

- Update puppet-cfssl (2.1.0 instead off 2.0.0)
- Refactor: move Puppet files to a subdirectory


--------------------------------

## v1.0.0, 2022.11.30

First release


--------------------------------

## Template

```markdown
## <major>.<minor>.patch_DEV     (unreleased)

### Added

### Changed

### Fixed

### Security

--------------------------------

```
