[![Last release](https://gitlab.adullact.net/adullact/pki/vagrant-puppet-cfssl/-/badges/release.svg)](https://gitlab.adullact.net/adullact/pki/vagrant-puppet-cfssl/-/releases)
[![Pipelines](https://gitlab.adullact.net/adullact/pki/vagrant-puppet-cfssl/badges/main/pipeline.svg)](https://gitlab.adullact.net/adullact/pki/vagrant-puppet-cfssl/pipelines?ref=main)
[![License : AGPL v3](https://img.shields.io/badge/License-AGPL3-blue.svg)](LICENSE.txt)
[![Code of Conduct](https://img.shields.io/badge/Contributor-Code%20of%20conduct-blueviolet.svg?style=flat-square)](CODE_OF_CONDUCT.md)
[![Contributing Welcome](https://img.shields.io/badge/Contributing-Welcome-fc7700.svg?style=flat-square)](CONTRIBUTING.md)

# Vagrant CFSSL

[[_TOC_]]

## About

This project lets you build from scratch and in few minutes
a running [**CFSSL** server](https://github.com/cloudflare/cfssl)
in a virtual machine on your laptop.

- [Changelog](CHANGELOG.md)
- [Contributing](CONTRIBUTING.md)
- [Code of Conduct](CODE_OF_CONDUCT.md)

### License

[**AGPL v3** - GNU Affero General Public License](LICENSE)

### Warning ⚠️ : only for test or demonstration purposes

> - Created **only** for **test** or **demonstration** purposes.
> - **No security** has been made for production.


## Technical details

### HTTP ports and URL

| Service             | Port | URL                                       |
|---------------------|------|-------------------------------------------|
| **CFSSL** API       | 8888 | `http://127.0.0.1:8888/api/v1/cfssl/info` |


### Database

| PostgreSQL          | Port | Database             | User             | Password                      |
|---------------------|------|----------------------|------------------|-------------------------------|
| **CFSSL** database  | 5432 | `cfssl_pg_database`  | `cfssl_pg_user`  | `cfssl_pg_password_ChangeIt`  |

Access to **CFSSL** database from your IDE:
> `postgresql://cfssl_pg_user:cfssl_pg_password_ChangeIt@localhost:5432/cfssl_pg_database?serverVersion=14&charset=utf8`


---------------------------------

## Documentation

### Prerequisites

On your computer you need to have installed:

- [Vagrant](https://www.vagrantup.com/downloads)
- [VirtualBox](https://www.virtualbox.org/)
- Git
- a Ruby version management tool like RVM or [RBenv](https://github.com/rbenv/rbenv)
- r10k (`gem install r10k`)
- Internet access
- Bash

For Linux users (debian, ubuntu), do *not* install Vagrant and VirtualBox from `apt-get` (packages are way too old),
download `.deb` from the respective websites.


---------------------------------

### Setup

Three steps to get you own running CFSSL on your laptop.

#### Step 1 - Clone this repository

```bash
git clone https://gitlab.adullact.net/adullact/pki/vagrant-cfssl.git
cd  vagrant-cfssl
```

#### Step 2 - Download all required Puppet modules used to configure the virtual machine

```bash
# To be executed in the directory containing the Vagrantfile

# Download all required Puppet modules used to configure the virtual machine
./BUILD.sh
```

#### Step 3 - Boot a virtual machine, download and configure all required items to get a running CFSSL

Some **CFSSL** configuration values
are presents in [`puppet/hieradata/parameters.yaml`](puppet/hieradata/parameters.yaml) file.

> You can modify this file to change value of any parameter documented by:
>
> - [**Puppet** component module **CFSSL**](https://gitlab.adullact.net/adullact/puppet-cfssl/-/blob/main/REFERENCE.md)


```bash
# To be executed in the directory containing the Vagrantfile

# Creates and starts the VM (Virtual Machine) according to the Vagrantfile
vagrant destroy -f  # stops the running machine Vagrant and destroys all resources
vagrant up          # Then you wait few minutes
                    # depends on your network access and power of your computer

# Stops gracefully the VM
vagrant halt

# Restart the VM
vagrant up

# Stops the VM and destroys all resources
# that were created during the machine creation process.
vagrant destroy -f
```

#### How to start the VM with a custom CFSLL port or a custom CFSSL IP address?

```bash
# Creates and starts the VM (Virtual Machine)
# with some customizations (ports, ip, ...)
# - customize PostgreSQL port  ---> 5432      (default, port allowed above 1000)
# - customize CFSSL port       ---> 8888      (default, port allowed above 1000)
# - customize CFSSL IP address ---> 127.0.0.1 (default)
VAGRANT_HOST_POSTGRESQL_PORT=5439  \
VAGRANT_HOST_CFSSL_PORT=8888       \
VAGRANT_HOST_CFSSL_IP=0.0.0.0      \
vagrant up
```


---------------------------------

### CFSSL - Usage documentation

1. Obtain public certificate of certification authority (CA)
2. Generate a new private key and certificate
3. **TODO** Get information about a certificate
4. **TODO** Revoke a certificate

#### 1. Get public certificate of certification authority (CA)

[CFSSL documentation, API endpoint `/api/v1/cfssl/info`](https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_info.txt)

```bash
# /api/v1/cfssl/info
# ---> Get information about certification authority (CA), including the CA certificate
curl -s       \
     -d '{}'  \
     -H "Content-Type: application/json"  \
     -X POST 127.0.0.1:8888/api/v1/cfssl/info | jq

# /api/v1/cfssl/info
# ---> Get public certificate of certification authority (CA)
curl -s       \
     -d '{}'  \
     -H "Content-Type: application/json"  \
     -X POST 127.0.0.1:8888/api/v1/cfssl/info | jq -r '.result' | jq -r '.certificate'
```

Example of an API response:

```json
{
  "success": true,
  "result": {
    "certificate": "-----BEGIN CERTIFICATE-----\nMIIDuDCCAqCgAwIBAgIUeYCALLsE4HkgGtrG+/lFFUK65gEwDQYJKoZIhvcNAQEL\nBQAwXjELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK\nEwhBRFVMTEFDVDEmMCQGA1UEAxMdVkFHUkFOVCBBRFVMTEFDVCBST09UIENBIEdF\nTjQwHhcNMjIxMTMwMDA0NDAwWhcNMjUxMTI5MDA0NDAwWjBmMQswCQYDVQQGEwJG\nUjEUMBIGA1UEBxMLTU9OVFBFTExJRVIxETAPBgNVBAoTCEFEVUxMQUNUMS4wLAYD\nVQQDEyVWQUdSQU5UIEFEVUxMQUNUIElOVEVSTUVESUFURSBDQSBHRU4xMIIBIjAN\nBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1Ne4s8rWSRP6M2nUrC4OaJl/Fdrv\nKvcgileIbLL8kgISQ53QZh7dxdQR8XnjcI3RHS0kwUZmsTj2UrXz+O5jr+N/0kv6\nHHqG9orltcK9qkuGXQFxiZjLDmva9z6MH02ZlqO+3sfI/NNKzqI1ajZLsvMMGclm\neCNhph2YbeOr4kK90qmeL/3NSzLIAyZCXdQkupOW3plzCSnWohuru8TdUTr0pyt0\nt1MDV3p2wYjPEIgzUPQ9z+qaCx1caXRMOLkKwBwAep3ColBoxEq3A+eTMXGToJ/m\ncD4idRLrT5Sr7PrZvFNuQuMK0w0FzUL69g64uD1c66SAGNrzvaKaB8RBtwIDAQAB\no2YwZDAOBgNVHQ8BAf8EBAMCAQYwEgYDVR0TAQH/BAgwBgEB/wIBATAdBgNVHQ4E\nFgQUMQkkiAQG/CgBOTMODyMoZsxfDMEwHwYDVR0jBBgwFoAUVZH9RbrYSgB7Kwjb\n/kGHl0SgIK0wDQYJKoZIhvcNAQELBQADggEBAKrp2v3biB1j4hGFtMmla1dj6Hd9\nPfhJCpU3SiYxaQEY7EeLBSju8GxQE1wuDeh7iq1IxkTBk+3nLhRVY81vBdwHByce\nu3o6Ex+E03qXrIS4PIx/PjSBpPf6QZF9oeXtbOFxWwuTlZ+sa4aLz1ZJHqaB6eah\nHOgt7Cwd3v1rIEHDDDdgf9duebfyz7dFzqAjIoxK+0mABTgpBVFkbj/KAdwQPWEd\nmoiuiGM4tLQUG8X5CCwViz0MlWguWCgGxlMQd/S1Wdgt3M7jU2DBcmK7iOMGmpgp\njBrmYqYlvQQ4JuQlqvbJCPLcv4P8dlyQlFD5B7jm4Rx2CJSn14XdY+Y/vck=\n-----END CERTIFICATE-----",
    "usages": [
      "client auth"
    ],
    "expiry": "8760h"
  },
  "errors": [],
  "messages": []
}

```

#### 2. Generate a new private key and certificate

[CFSSL documentation, API endpoint `/api/v1/cfssl/newcert`](https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_newcert.txt)

```bash
# /api/v1/cfssl/newcert
# ---> Generate a new private key and certificate
REQUEST='{
    "request": {
        "hosts": [
            ""
        ],
        "key": {
            "algo": "rsa",
            "size": 2048
        },
        "names": [
            {
                "C": "FR",
                "ST": "Herault",
                "L": "Montpellier",
                "O": "Exemple.org",
                "OU": "IT department"
            }
        ],
        "CN": "john.doe@exemple.org"
    }
}'
curl -s                \
     -d  "${REQUEST}"  \
     -H "Content-Type: application/json"  \
     -X POST 127.0.0.1:8888/api/v1/cfssl/newcert | jq
```

Example of an API response:

```json
{
  "success": true,
  "result": {
    "certificate": "-----BEGIN CERTIFICATE-----\nMIIEHDCCAwSgAwIBAgIUGjaKBbINOHYGxvsI5nFSf64T4i4wDQYJKoZIhvcNAQEL\nBQAwZjELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK\nEwhBRFVMTEFDVDEuMCwGA1UEAxMlVkFHUkFOVCBBRFVMTEFDVCBJTlRFUk1FRElB\nVEUgQ0EgR0VOMTAeFw0yMjEyMDIwMDAwMDBaFw0yMzEyMDIwMDAwMDBaMIGCMQsw\nCQYDVQQGEwJGUjEQMA4GA1UECBMHSGVyYXVsdDEUMBIGA1UEBxMLTW9udHBlbGxp\nZXIxFDASBgNVBAoTC0V4ZW1wbGUub3JnMRYwFAYDVQQLEw1JVCBkZXBhcnRtZW50\nMR0wGwYDVQQDDBRqb2huLmRvZUBleGVtcGxlLm9yZzCCASIwDQYJKoZIhvcNAQEB\nBQADggEPADCCAQoCggEBANAKkYcy9E+1iVdT7ZxfYalXc4f9+aQXqHIBYD6WEfzJ\nv0jgFfIqkNbSr+c9y7IsCUFQpBoN9oMjrGH0b6Y6v5KRE2gPl4tW3+cGbIOs6cqD\nNrRPMaRAEFtdid3xQ9fGOLpHOnveOv8NnPdIfhJ+WvZ5fr5kVqcrazwI7F266ynw\nGjrTqdAlxRvO1JSVcHB+4Q1djsX1AQhchhx4Ep8cw6sa8mo5ITMc5RSKq6fUveQ6\ne/myNw2qlBdfgpvVtDpkJ7Ty90uAVE6D5Gc+s8tLhYqmQ5ygrDz/FythKZC7a/vf\noTy6blavKXBfYgLn3wzMYzV02uQv77pmJmRp953keZ0CAwEAAaOBpDCBoTATBgNV\nHSUEDDAKBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBT3nkmxuIfg\nKobckelKU9U7zloO5jAfBgNVHSMEGDAWgBSy5tnCIHjsalRVRDznt1l6mVDnoTAL\nBgNVHREEBDACggAwLwYDVR0fBCgwJjAkoCKgIIYeaHR0cDovL3BraS5leGFtcGxl\nLm9yZy9jcmwucGVtMA0GCSqGSIb3DQEBCwUAA4IBAQARS03BBI/IhH7pVCL6jqUg\neoEckHq1+9P1L/j9gPKAk+EGdCt+bIrLFwvMfBiO336ICsdvCRcUk6Ai/uOlU1xs\nzemmfEvHQ9i4O+HaipUbe70FWyuWBLJY5LJKgC5qTlX/E1w3Pv7ST5/Q/ODLnkIs\nAfPRa0RDgLrTjNpp547ccULlV3UiDP5kXRuJhGENsgt+D9nfFyMM0yrjuVrfaFd3\nuDvhMynG/iPw6M/5n8QW+XzejD9IMkser2r7AEKYZQSXJXl+2Ihh2P65lJBb/urR\nkcSPMtXC1KVHxWUvEem6iK46u4USaltj965yfvZ645iHoMm5OOoGL1d+nMwRFJg5\n-----END CERTIFICATE-----\n",
    "certificate_request": "-----BEGIN CERTIFICATE REQUEST-----\nMIIC5jCCAc4CAQAwgYIxCzAJBgNVBAYTAkZSMRAwDgYDVQQIEwdIZXJhdWx0MRQw\nEgYDVQQHEwtNb250cGVsbGllcjEUMBIGA1UEChMLRXhlbXBsZS5vcmcxFjAUBgNV\nBAsTDUlUIGRlcGFydG1lbnQxHTAbBgNVBAMMFGpvaG4uZG9lQGV4ZW1wbGUub3Jn\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0AqRhzL0T7WJV1PtnF9h\nqVdzh/35pBeocgFgPpYR/Mm/SOAV8iqQ1tKv5z3LsiwJQVCkGg32gyOsYfRvpjq/\nkpETaA+Xi1bf5wZsg6zpyoM2tE8xpEAQW12J3fFD18Y4ukc6e946/w2c90h+En5a\n9nl+vmRWpytrPAjsXbrrKfAaOtOp0CXFG87UlJVwcH7hDV2OxfUBCFyGHHgSnxzD\nqxryajkhMxzlFIqrp9S95Dp7+bI3DaqUF1+Cm9W0OmQntPL3S4BUToPkZz6zy0uF\niqZDnKCsPP8XK2EpkLtr+9+hPLpuVq8pcF9iAuffDMxjNXTa5C/vumYmZGn3neR5\nnQIDAQABoB4wHAYJKoZIhvcNAQkOMQ8wDTALBgNVHREEBDACggAwDQYJKoZIhvcN\nAQELBQADggEBAIZXd8x9QXlKl0HNzHh6dPTlK8PhotD+gx4wK96OX5/Vo7Js3avd\nw3Aw/rX0cK0JlzlG6+3Xzd2yeqkYL88s+3sZQ5hTxlMPf8LhbK8YdpZ3mjlAoqqS\nHKRE1kf7uk2jXB6OD9j2VZEcY3UeeVazyE/XIGUN+GuS8TLiMdhT9/9zx7TVb/7+\n/r3Zh9WYTQMEdd6hfPZUQVDOaSegxg2Q8tW2J3JDC4YZCthvAJtu8+RRdw1kjxH0\nAMg4JkpDZt+odVYmmbJZCTTXjqQ2Vxn2NIL+wH6yHRmtMwt5J84dj3z7MC9eHMWV\nVokrJqa9uT+wjey0djOYY/MNVzq+i3YmZUE=\n-----END CERTIFICATE REQUEST-----\n",
    "private_key": "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEA0AqRhzL0T7WJV1PtnF9hqVdzh/35pBeocgFgPpYR/Mm/SOAV\n8iqQ1tKv5z3LsiwJQVCkGg32gyOsYfRvpjq/kpETaA+Xi1bf5wZsg6zpyoM2tE8x\npEAQW12J3fFD18Y4ukc6e946/w2c90h+En5a9nl+vmRWpytrPAjsXbrrKfAaOtOp\n0CXFG87UlJVwcH7hDV2OxfUBCFyGHHgSnxzDqxryajkhMxzlFIqrp9S95Dp7+bI3\nDaqUF1+Cm9W0OmQntPL3S4BUToPkZz6zy0uFiqZDnKCsPP8XK2EpkLtr+9+hPLpu\nVq8pcF9iAuffDMxjNXTa5C/vumYmZGn3neR5nQIDAQABAoIBABJjrMN3pJLbKB25\nBHrbD4XpfgPaVlPD55bUOk6t8z8WEjt921LBonXqv7PACZ5hOBw25ZqL+2tSHu21\nrpzR+pcZDc8X+bxOXJJORg9JnIzTFmXPzkKHiyqYVRRyPB35npAQtcG3Ph3HdUWC\neepmkuHQkXyA4Av80YjH/SS33guhzdCpa+R5A+joOPAyGrLYQZtIOW91ieV8eTma\npgilZh7vAArtradDtoGDYAEhZ+3KlvAxQvmeI/31QBCx6yC1AZsZVViuBbz4nA3Z\n+LL37BIkw0mTOOQ19qgKgTI7Aog/NLu+sN8E/TNtJsUKBMblvs8IPprjT1PRXEsf\nP4cE7MECgYEA4bcILZl+tbsJMvAnxCots3Uhw9GZuVNkivc+0c4mUVZ0eyoN4JlE\nRSOaNfZb2awvMgqRh9IrRQUpsjisjub/IxRYeg/ww4o9ixNyv2lsUEj8EBZD0uqc\nmP0XYi4Yn2SX10tFmMhe6vFs6C3z1OkjMJjbKo/K0XuAVAnIne0L7s0CgYEA6/R4\n3gNwYOoRg8Aia34Wph/ogTaZtOcFYj19gCEbRKqYz9AoC3xPa8kVg2oVz+HZJMdk\nEZuFEivUl5qfGaGyEuWAFlrU+LRwX6ul1mEMi+mkqIdEp9pHf3ZQFnSGN8mgN2g5\nSAYqlVuIzlEvLPc5wqQQ+h+Lxqcookv6odfkFhECgYEA1kKLpdWWPWZthggIh6LO\nPRMMWTjnJtAtmei3niuFniJmcd/qynBTKRpxsZXJfOiA6Iy1tutfnaYX/ZDk5MYM\nHT2b+0qUbtoUL6fnfiHR6qhNdevcuCNzQZrGjrFZOD0iEgHm7/AMghNpIUE1u78P\nY3tGWLGkgpQsrhmXcapqcGUCgYBe/5TSPiw2uMbIdsjk4mUYCWzA6UzFSuYeAYfP\n2IR0BwNzV+lnhmsriFBDEqkpg1K7vhOKC0VuZXs4dqZr2XAqr3/8gCGS6IL94cvp\nHjXA9xa2S4/WzMlQhkAEanHHcEQzSqEVYHYwoCPafVEzF26H8SpsqdrK5m4zucd0\n9cj9IQKBgC/KPYiumfyhUsp/NjYoISO2RHJB9rJf7m01ISWJKv/VsvyfGz0yY9xe\ngFEKKNix1FSdWVpWKtb/c7+lcIESca/wYQB0XPhuWJwPETgUW+WtppAgopt/LIO8\nQ82i8MgelO6JXvrfT893i9URHbj/GEkfYoHeQhOqZrYVhGMt4r1h\n-----END RSA PRIVATE KEY-----\n",
    "sums": {
      "certificate": {
        "md5": "E069F854A1EB921CF16FBEB21D72CEFD",
        "sha-1": "B3AE4F837D242DD1299AEF1DE6534039335FB19E"
      },
      "certificate_request": {
        "md5": "25C86B8F8BCFD0D2A9A6DEE73CB3AE1F",
        "sha-1": "82C72074AB85B4D1BED885AB946520F56F002F72"
      }
    }
  },
  "errors": [],
  "messages": []
}
```
